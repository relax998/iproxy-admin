import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'

Vue.use(Vuex)

const store = new Vuex.Store({
  // 项目所有数据放到子模块中，采用了模块形式管理共享状态
  modules: {
    app, // 折叠与展开
    settings, // logo和头部
    user// 用户信息
  },
  // getters中放了各子模块的快速访问
  getters
})

export default store
