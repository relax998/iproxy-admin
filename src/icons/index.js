import Vue from 'vue'
import SvgIcon from '@/components/SvgIcon'// svg component

// register globally注册svg全局icon组件
Vue.component('svg-icon', SvgIcon)

// svg目录中所有.svg结尾的文件为icon图标
const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)
// 以上两行将svg目录中所有svg后缀文件引入到项目中
