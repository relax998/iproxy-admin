module.exports = {

  // 项目标题字符串
  title: 'iProxy Admin',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header 头部固定
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar 是否显示侧边栏logo
   */
  sidebarLogo: false
}
